
const wd = require('wd')
const assert = require('assert')
const LogenPage = require('./POM/logenPage_pom')
const SmenaPage = require('./POM/smena_pom')

class SmenaTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.smenaPage = new SmenaPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }
  
  async runSmenaTest() {
    console.log('Starting smena test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.smenaPage.verifyFields();
    await this.smenaPage.createShift();
    const expectedValue = true; // expected button state should be disabled
    const actualValue1 = await this.smenaPage.isButtonDisabled('Создать заявку','Сделать инкаccацию','Закрыть кассу'); // replace buttonName with the name of your button
    assert.strictEqual(actualValue1, expectedValue);
    await this.smenaPage.creatCmena();
    const expectedValue2 = true; // expected button state should be
    const activeValue1 = await this.smenaPage.isButtonActive("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[1]/Button[2]");
    assert.strictEqual(activeValue1, expectedValue2);
    await this.smenaPage.creatCmenabutton();
    await this.smenaPage.closeShift();
    await this.smenaPage.filter();
    await this.logenPage.close();
    console.log('Smena POSITIVE test completed!');
  }
}

module.exports = SmenaTestCaller;