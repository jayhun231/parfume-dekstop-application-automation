const wd = require('wd')
const LogenPage = require('./POM/logenPage_pom')

const username = 'medion_admin'
const password = 'admin_medion123'

async function runTest() {
  const driver = wd.promiseChainRemote({
    host: 'https://oauth-jayhun231-bca2b:a78c66f1-7851-4a2a-9aec-8d3678be3e13@ondemand.eu-central-1.saucelabs.com:443/wd/hub',
    port: 80,
    user: 'oauth-jayhun231-bca2b',
    pwd: 'a78c66f1-7851-4a2a-9aec-8d3678be3e13',
  });
  const logenPage = new LogenPage(driver);

  console.log('Starting login test...')

  await logenPage.open()
  await logenPage.login(username, password)

  await logenPage.close()
  console.log('Login test completed!')
}

runTest()