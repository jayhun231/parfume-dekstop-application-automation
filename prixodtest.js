const wd = require('wd')
const LogenPage = require('./POM/logenPage_pom')
const PrixodPage = require('./POM/prixod_pom')

class PrixodTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.prixodPage = new PrixodPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runPrixodTest() {
    console.log('Starting Prixod test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.prixodPage.add_prixod()
    await this.prixodPage.filter_prixod()
    await this.logenPage.close();
    console.log('Prixod test completed!');
}
}

module.exports = PrixodTestCaller;