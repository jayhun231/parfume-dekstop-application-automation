class ReturnsPage {
    constructor(driver) {
      this.driver = driver
    }

    async creat_new_returns () {
        const returnsIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[8]");
        await returnsIcon.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addExpenses = await this.driver.elementByName('Добавить');
        await addExpenses.click();
        const addproduct = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Button[2]");
        await addproduct.click()
        const addproduct0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await addproduct0.click();
        const addproduct2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Text[2]");
        await addproduct2.click();
        const addproduct1 = await this.driver.elementByName("Отсканируйте штрих-код");
        await addproduct1.sendKeys("3348901420402");
        await new Promise(resolve => setTimeout(resolve, 3000))
        const sale = await this.driver.elementByName("Сохранить");
        await sale.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const sale0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[2]");
        await sale0.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const sale1 = await this.driver.elementByName("Совершить возврат");
        await sale1.click();
        await new Promise(resolve => setTimeout(resolve, 3000))

    }

    async continue_returns () {
      const returnsIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[8]");
      await returnsIcon.click();
      await new Promise(resolve => setTimeout(resolve, 3000))
      const addExpenses = await this.driver.elementByName('Добавить');
      await addExpenses.click();
      const addproduct = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Button[2]");
      await addproduct.click()
      const addproduct0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await addproduct0.click();
      const sale = await this.driver.elementByName("Сохранить");
      await sale.click();
      await new Promise(resolve => setTimeout(resolve, 3000))
      const sale0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[2]");
      await sale0.click();
      await new Promise(resolve => setTimeout(resolve, 3000))
      const sale1 = await this.driver.elementByName("Продажа");
      await sale1.click();
      await new Promise(resolve => setTimeout(resolve, 3000))

    }

    async save () {
        const returnsIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Text[8]");
        await returnsIcon.click();
        const returnsCon = await this.driver.elementByName('В процессе');
        await returnsCon.click();
        const consultante = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Button[1]");
        await consultante.click();
        const name = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await name.click();
        const saveButton = await this.driver.elementByName('Сохранить');
        await saveButton.click();

    }

    async filter_positive () {
        const returnsIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Text[8]");
        await returnsIcon.click();
        const filterinput = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group/Button[1]")
        await filterinput.click();
        const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filter.click();
        const filter0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filter0.click();
            const fields = [
              "/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[6]",
              "/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[12]"
            ]
        
            for (const field of fields) {
              const element = await this.driver.elementByXPath(field)
              const text = await element.text()
        
              if (text === 'В процессе') {
                console.log(`Should be visualized founded items with 'В процессе'.`)
              } else {
                console.error(`Should be visualized not founded items with 'В процессе' is incorrect: ${text}`)
              }
            }
            // const createShiftStatus2 = await this.driver.elementByName('Выберите дату');
            // await createShiftStatus2.click();
            // await new Promise(resolve => setTimeout(resolve, 3000));
            // const may = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[4]");
            // await may.click();
            // const may1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[8]");
            // await may1.click();
            // const prinyat = await this.driver.elementByName("Принять");
            // await prinyat.click();
            const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
            await logout.click();
            await new Promise(resolve => setTimeout(resolve, 2000))
            const logout1 = await this.driver.elementByName("Да");
            await logout1.click(); 
            await new Promise(resolve => setTimeout(resolve, 2000))

    }


}
  
module.exports = ReturnsPage