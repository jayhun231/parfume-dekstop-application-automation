const wd = require('wd')
const Promise = require('bluebird')

class LogenPage {
  constructor(driver) {
    this.driver = driver
  }

  async open() {
    console.log('Opening app...')
    await this.driver.init({
      platformName: 'Windows',
      app: 'C:\\Program Files (x86)\\Release\\parfume_cashier_desktop',
      deviceName: 'WindowsPC',
    })
    
    console.log('App opened!')
    await new Promise(resolve => setTimeout(resolve, 10000))
  }

  async close() {
    console.log('Closing app...')
    await this.driver.quit()
    console.log('App closed!')
  }

  async login(username, password) {
    const fullscreen = await this.driver.elementByName('Развернуть')
    const usernameInput = await this.driver.elementByName('Введите имя пользователя')
    const passwordInput = await this.driver.elementByName('Введите пароль')
    const loginButton = await this.driver.elementByName('Войти')

    await fullscreen.click()
    await usernameInput.sendKeys(username)
    await passwordInput.sendKeys(password)
    await loginButton.click()
    await new Promise(resolve => setTimeout(resolve, 5000))
    console.log('Logged in!')
  }

}

module.exports = LogenPage