class KonsultantPage {
    constructor(driver) {
      this.driver = driver
    }

    async creat_new_konsultant (){
        const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[5]");
        await costumButton.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addcostum = await this.driver.elementByName('Добавить')
        await addcostum.click();
        const addname = await this.driver.elementByName('Введите фамилию');
        await addname.sendKeys("Test");
        const addname0 = await this.driver.elementByName('Введите имя');
        await addname0.sendKeys("Tester");; 
        const addname1 = await this.driver.elementByName('Введите отчество');
        await addname1.sendKeys("Testerov");;
        const bhDate = await this.driver.elementByName('Выберите дату рождения')
        await bhDate.click();
        const bhDate0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Text");
        await bhDate0.click();
        await bhDate0.click();
        await bhDate0.click();
        const bhDate1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[1]");
        await bhDate1.click();
        await bhDate1.click();
        const bhDate2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[2]");
        await bhDate2.click()
        const bhDate3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[22]");
        await bhDate3.click();
        const printDate = await this.driver.elementByName('Принять');
        await printDate.click();
        const addelement = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[2]");
        await addelement.click();
        const addelement0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await addelement0.click();
        const addelement1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[3]");
        await addelement1.click();
        const addelement2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await addelement2.click();
        const addelement3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await addelement3.sendKeys("Olmazor");
        const addelement4 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[4]");
        await addelement4.sendKeys("Olmazor");
        const addelement5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[5]");
        await addelement5.sendKeys("+998998997272");
        const addelement6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[6]");
        await addelement6.sendKeys("+998998997272");
        const addelement7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[4]");
        await addelement7.click();
        const addelement9 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await addelement9.click();
        const addelement10 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[1]");
        await addelement10.sendKeys("998998997272");
        const addelement11 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[2]");
        await addelement11.sendKeys("AA");
        const addelement12 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[3]");
        await addelement12.sendKeys("998998997272");
        const addelement13 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Text");
        await addelement13.click();
        const addelement14 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[2]");
        await addelement14.click();
        const addelement15 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[31]");
        await addelement15.click();
        const addelement16 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[4]");
        await addelement16.click();
        const addelement17 = await this.driver.elementByName('Добавить')
        await addelement17.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addelement18 = await this.driver.elementByName("Назад")
        await addelement18.click(); //
    }

    async edit_new_konsultant (){
      const editinput = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[2]");
      await editinput.click();
      const editinput0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group/Edit[1]");
      await editinput0.sendKeys("rov");
      await new Promise(resolve => setTimeout(resolve, 3000))
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[5]");
      await costumButton.click();

    }

    async filter_konsultant (){
      const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[1]");
      await filter.click();
      const filter0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await filter0.click();
      const filter1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
      await filter1.click();
      const filter2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[6]");
      const text = await filter2.text()

      if (text === 'Korzinka-Аэропорт') {
        console.log(`Find correct client 'Korzinka-Аэропорт'.`)
      } else {
        console.error(`Text content of 'Korzinka-Аэропорт' is incorrect: ${text}`)
      }

      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async creat_new_konsultantng (){
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[5]");
      await costumButton.click();
      await new Promise(resolve => setTimeout(resolve, 3000))
      const addcostum = await this.driver.elementByName('Добавить')
      await addcostum.click();
      const addname = await this.driver.elementByName('Введите фамилию');
      await addname.sendKeys("");
      const addname0 = await this.driver.elementByName('Введите имя');
      await addname0.sendKeys(" ");; 
      const addname1 = await this.driver.elementByName('Введите отчество');
      await addname1.sendKeys("Testerov");;
      const bhDate = await this.driver.elementByName('Выберите дату рождения')
      await bhDate.click();
      const bhDate0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Text");
      await bhDate0.click();
      await bhDate0.click();
      await bhDate0.click();
      const bhDate1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[1]");
      await bhDate1.click();
      await bhDate1.click();
      const bhDate2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[2]");
      await bhDate2.click()
      const bhDate3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[22]");
      await bhDate3.click();
      const printDate = await this.driver.elementByName('Принять');
      await printDate.click();
      const addelement = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[2]");
      await addelement.click();
      const addelement0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await addelement0.click();
      const addelement1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[3]");
      await addelement1.click();
      const addelement2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await addelement2.click();
      const addelement3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await addelement3.sendKeys("Olmazor");
      const addelement4 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[4]");
      await addelement4.sendKeys("Olmazor");
      const addelement5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[5]");
      await addelement5.sendKeys("+998998997272");
      const addelement6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Edit[6]");
      await addelement6.sendKeys("+998998997272");
      const addelement7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[1]/Button[4]");
      await addelement7.click();
      const addelement9 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await addelement9.click();
      const addelement10 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[1]");
      await addelement10.sendKeys("998998997272");
      const addelement11 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[2]");
      await addelement11.sendKeys("AA");
      const addelement12 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Edit[3]");
      await addelement12.sendKeys("998998997272");
      const addelement13 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group[2]/Text");
      await addelement13.click();
      const addelement14 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[2]");
      await addelement14.click();
      const addelement15 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[31]");
      await addelement15.click();
      const addelement16 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[4]");
      await addelement16.click();
      const addelement17 = await this.driver.elementByName('Добавить')
      await addelement17.click();
      
      await new Promise(resolve => setTimeout(resolve, 3000))
      const addelement18 = await this.driver.elementByName("Назад")
      await addelement18.click(); //
      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000));
  }

}
  
module.exports = KonsultantPage