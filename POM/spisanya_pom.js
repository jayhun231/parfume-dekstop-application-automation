class SpisanyaPage {
    constructor(driver) {
      this.driver = driver
    }

    async add_spisanya () {
        const click_Icon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[14]");
        await click_Icon.click();
        await new Promise(resolve => setTimeout(resolve, 2000));
        const clickadd = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[2]");
        await clickadd.click();
        const clickadd0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button");
        await clickadd0.click();
        await new Promise(resolve => setTimeout(resolve, 2000));
        const clickadd1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await clickadd1.click();
        const clickadd2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await clickadd2.click();
        await new Promise(resolve => setTimeout(resolve, 1000));
        const clickadd3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group[2]/Group/Button");
        await clickadd3.click();
        const clickadd4 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group[2]/Group/Group[1]/Button");
        await clickadd4.click();
        await new Promise(resolve => setTimeout(resolve, 1000));
        const clickadd5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await clickadd5.click();
        await new Promise(resolve => setTimeout(resolve, 1000));
        const clickadd6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group[1]/Button[2]");
        await clickadd6.click();
        const clickadd7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[3]/Group/Group/Button");
        await clickadd7.click();
        await new Promise(resolve => setTimeout(resolve, 2000));

    }

    async filter_spisanya () {
        await new Promise(resolve => setTimeout(resolve, 1000));
        const click_Icon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[14]");
        await click_Icon.click();
        const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[1]");
        await filter.click();
        const filter0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filter0.click();
        const filter1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filter1.click();
        const element = await this.driver.elementByName("Завершён");
        const text = await element.text();
        if (text === 'Завершён') {
          console.log(`Text content of 'Завершён' is correct.`);
        } else {
          console.error(`Text content of 'Завершён' is incorrect: ${text}`);
        }

        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))

    }

}
  
module.exports = SpisanyaPage