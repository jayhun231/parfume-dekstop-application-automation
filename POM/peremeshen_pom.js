class PermeshinPage {
    constructor(driver) {
      this.driver = driver
    }

    async add_permeshin () {
        const perimeshinIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[9]");
        await perimeshinIcon.click();
        const addPermishen = await this.driver.elementByName("Добавить");
        await addPermishen.click();
        const filialcheck = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button");
        await filialcheck.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const filialcheck0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filialcheck0.click();
        const addPermishen0 = await this.driver.elementByName("Создать");
        await addPermishen0.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const codeProduct = await this.driver.elementByName("Отсканируйте или введите штрих-код");
        await codeProduct.sendKeys("3348901420402");
        await new Promise(resolve => setTimeout(resolve, 5000))
        const codeProduct2 = await this.driver.elementByName("Отсканируйте или введите штрих-код");
        await codeProduct2.sendKeys("3348901420402");
        const Button= await this.driver.elementByName("Отправить");
        await Button.click();

    }

    async prinyat () {
        const perimeshinIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[9]");
        await perimeshinIcon.click();
        const filialcheck = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group/Text[2]");
        await filialcheck.click();
        const filialcheck0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[2]/Group/Group/Text[2]");
        await filialcheck0.click();
        const codeProduct = await this.driver.elementByName("Отсканируйте или введите штрих-код");
        await codeProduct.sendKeys("3348901420402");
        await new Promise(resolve => setTimeout(resolve, 3000))
        const codeProduct0 = await this.driver.elementByName("Отсканируйте или введите штрих-код");
        await codeProduct0.sendKeys("3348901420402");
        const Button = await this.driver.elementByName("Принять");
        await Button.click();
    }
     
    async permeshin_filter () {
        const perimeshinIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[9]");
        await perimeshinIcon.click();
        const filter0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Button[1]");
        await filter0.click();
        const filter1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filter1.click();
        const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filter.click();
        // const filterdata = await this.driver.elementByName("Выберите дату");
        // await filterdata.click();
        // const filterdata0= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[2]");
        // await filterdata0.click();
        // const filterdata1= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[19]");
        // await filterdata1.click();
        // const prinyat = await this.driver.elementByName("Принять");
        // await prinyat.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const perimeshinIcon0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[9]");
        await perimeshinIcon0.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const delitebutton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[2]/Group/Group/Group/Button");
        await delitebutton.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByName("Да");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000));

    }

}
  
module.exports = PermeshinPage