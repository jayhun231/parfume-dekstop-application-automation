class VozvratPage {
    constructor(driver) {
      this.driver = driver
    }
    
    // async verifyFields() {
    //   const fields = [
    //     'ID-Возврата',
    //     'Кассир',
    //     'Филиал в',
    //     'Статус',
    //     'Дата и время',
    //     'Статус',
    //   ]
  
    //   for (const field of fields) {
    //     const element = await this.driver.elementByName(field)
    //     const text = await element.text()
  
    //     if (text === field) {
    //       console.log(`Text content of ${field} is correct.`)
    //     } else {
    //       console.error(`Text content of ${field} is incorrect: ${text}`)
    //     }
    //   }
    // }

    async click_icon () {
        const clickIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[13]");
        await clickIcon.click();
    }
    
    // async click_table () {
    //     const clicktable = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[2]");
    //     await clicktable.click();
    //     const fields = [
    //         'Штрих-код',
    //         'Название',
    //         'Количество',
    //         'Остаток',
    //         'Цена',
    //       ]
      
    //       for (const field of fields) {
    //         const element = await this.driver.elementByName(field)
    //         const text = await element.text()
      
    //         if (text === field) {
    //           console.log(`Text content of ${field} is correct.`)
    //         } else {
    //           console.error(`Text content of ${field} is incorrect: ${text}`)
    //         }
    //       }
    //     const clickIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[13]");
    //     await clickIcon.click();

    // }

    async add_vozvrat () {
        const add_vozvrat0 = await this.driver.elementByName("Создать");
        await add_vozvrat0.click();
        const add_vozvrat1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button");
        await add_vozvrat1.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const add_vozvrat2= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await add_vozvrat2.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const add_vozvrat3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await add_vozvrat3.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const codeProduct0 = await this.driver.elementByName("Отсканируйте или введите штрих-код");
        await codeProduct0.sendKeys("3348901420402");
        const add_vozvrat7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Button[1]");
        await add_vozvrat7.click();
        const Button= await this.driver.elementByName("Отправить");
        await Button.click();

    }

    async delite_vozvrat () {
        const clickIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[13]");
        await clickIcon.click();
        const clickdelite = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Button");
        await clickdelite.click();
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))
    }

    async negative_vozvrat () {
        const clickIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[13]");
        await clickIcon.click();
        const add_vozvrat0 = await this.driver.elementByName("Создать");
        await add_vozvrat0.click();
        const add_vozvrat1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button");
        await add_vozvrat1.click();
        const add_vozvrat2= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await add_vozvrat2.click();
        const add_vozvrat11 = await this.driver.elementByName("Создать");
        await add_vozvrat11.click();
        const add_vozvrat9 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Button[2]");
        await add_vozvrat9.click();
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))
    }

}
  
module.exports = VozvratPage