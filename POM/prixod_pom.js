class PrixodPage {
    constructor(driver) {
      this.driver = driver
    }

    async add_prixod () {
        const prixodicon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[10]");
        await prixodicon.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Button[2]");
        await addprixod.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button[1]");
        await addprixod0.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await addprixod1.click();
        const addprixod2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button[2]");
        await addprixod2.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await addprixod3.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod4 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[1]");
        await addprixod4.sendKeys("12345");
        const addprixod5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[2]");
        await addprixod5.sendKeys("123456");
        const addprixod6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[3]");
        await addprixod6.sendKeys("1234567");
        const addprixod7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await addprixod7.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod8 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[2]/Group/Text[2]");
        await addprixod8.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod9 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Edit");
        await addprixod9.sendKeys("783320482106");
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod10 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[4]/Group/Group/Button");
        await addprixod10.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const iconclick = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[10]");
        await iconclick.click();


    }

    async filter_prixod () {
        const filterpr = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Button[1]");
        await filterpr.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const filterpr1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filterpr1.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const filterpr2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filterpr2.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const iconclick = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[10]");
        await iconclick.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const filterpr3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Edit[1]/Edit");
        await filterpr3.sendKeys("0000010");
        await new Promise(resolve => setTimeout(resolve, 3000))
        const filterclear = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Edit[1]/Edit/Button");
        await filterclear.click();
        // const filterpr4 = await this.driver.elementByName("Дата открытия");
        // await filterpr4.click();
        // await new Promise(resolve => setTimeout(resolve, 3000))
        // const filterpr5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[1]");
        // await filterpr5.click();
        // await new Promise(resolve => setTimeout(resolve, 3000))
        // const filterpr6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[13]");
        // await filterpr6.click();
        // await new Promise(resolve => setTimeout(resolve, 3000))
        // const filterpr7 = await this.driver.elementByName("Принять");
        // await filterpr7.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async prixod_negativ () {
        const prixodicon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[10]");
        await prixodicon.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/Group/Button[2]");
        await addprixod.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button[1]");
        await addprixod0.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const addprixod1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await addprixod1.click();
        const filterpr7 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await filterpr7.click();
        const fields = [
            'Введите номер доставки',
            'Введите номер договора',
            'Введите номер счет фактуры',
          ]
      
          for (const field of fields) {
            const element = await this.driver.elementByName(field)
            const text = await element.text()
      
            if (text === field) {
              console.log(`Eror ${field} is correct.`)
            } else {
              console.error(`Eror ${field} is incorrect: ${text}`)
            }
          }

          const group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
          await group.click();


        await new Promise(resolve => setTimeout(resolve, 3000))
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))
    }

}
  
module.exports = PrixodPage