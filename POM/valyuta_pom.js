class ValyutaPage {
    constructor(driver) {
      this.driver = driver
    }

    async valyuta_add () {
        const addvalute = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[12]");
        await addvalute.click();
        const addvalute2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[2]");
        await addvalute2.click();
        const addvalute3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Button");
        await addvalute3.click();
        const addvalute4= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Edit");
        await addvalute4.sendKeys("s");
        await new Promise(resolve => setTimeout(resolve, 2000))
        const Addvalute5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await Addvalute5.click();
        // const addvalute5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        // await addvalute5.click();
        const addvalute6 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
        await addvalute6.click();

    }

    async filter_valute () {
        const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[1]");
        await filter.click();
        const filter1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filter1.click();
        const filter2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await filter2.click();
        const fields = [
            'KZT',
            'KZT',
            'KZT',
            'KZT',
            'KZT',
            'KZT',
            'KZT',
          ]
      
          for (const field of fields) {
            const element = await this.driver.elementByName(field)
            const text = await element.text()
      
            if (text === field) {
              console.log(`Text content of ${field} is correct.`)
            } else {
              console.error(`Text content of ${field} is incorrect: ${text}`)
            }
          }
          const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
          await logout.click();
          await new Promise(resolve => setTimeout(resolve, 2000))
          const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button[2]");
          await logout1.click(); 
          await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async negative_add () {
        const addvalute = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[12]");
        await addvalute.click();
        const addvalute2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Button/Button[2]");
        await addvalute2.click();
        const addvalute5 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await addvalute5.click();
        await new Promise(resolve => setTimeout(resolve, 5000))
        const element = await this.driver.elementByXPath('/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[2]/Text');
        const text = await element.text();
        if (text === 'Введите правильную сумму') {
          console.log(`Eror 'Введите правильную сумму'`);
        } else {
          console.error(`Eror 'Введите правильную сумму'`);
        }

        const group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
        await group.click();
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByName("Да");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))
    }

}
  
module.exports = ValyutaPage