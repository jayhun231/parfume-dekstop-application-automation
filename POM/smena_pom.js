
class SmenaPage {
    constructor(driver) {
      this.driver = driver
    }
    
    async verifyFields() {
      const fields = [
        'Смена-ID',
        'Филиал',
        'Кассир',
        'Статус',
        'Время открытия',
        'Время закрытия',
        'Инкассация сделана',
      ]
  
      for (const field of fields) {
        const element = await this.driver.elementByName(field)
        const text = await element.text()
  
        if (text === field) {
          console.log(`Text content of ${field} is correct.`)
        } else {
          console.error(`Text content of ${field} is incorrect: ${text}`)
        }
      }
    }

    async isButtonDisabled(buttonName) {
      const button = await this.driver.elementByName(buttonName);
      let tries = 0;
      while (tries < 3) {
        let isEnabled = await button.isEnabled();
        if (!isEnabled) {
          return true; // button is disabled
        }
        console.log('Button is disabled: ');
        await new Promise(resolve => setTimeout(resolve, 1000));
        tries++;
      }
      return true;
    }

    async isButtonActive(buttonName) {
      const button = await this.driver.elementByXPath(buttonName);
      let tries = 0;
      while (tries < 3) {
        let isEnabled = await button.isEnabled();
        if (isEnabled) {
          console.log('Button is active');
          return true; // button is active
        } 
        await new Promise(resolve => setTimeout(resolve, 1000));
        tries++;
      } 
      console.log('Button is not active');
      return false;
    }
    
    async createShift() {
        const createShiftButton = await this.driver.elementByName('Создать смену')
        await createShiftButton.click()
        await new Promise(resolve => setTimeout(resolve, 3000))
      }

      async creatCmena() {
        const cmena = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[1]");
        await cmena.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const cmenastatus = await this.driver.elementByName('В ожидании');
        await cmenastatus.click();
      }

      async creatCmenabutton() {
        const createShift2Button = await this.driver.elementByName('Открыть смену')
        await createShift2Button.click()
        const el1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Edit/Edit");
        await el1.sendKeys("1");
        const createShift3Button = await this.driver.elementByName('Отправить')
        await createShift3Button.click()
        await new Promise(resolve => setTimeout(resolve, 3000))
        try {
          // Elementni izlash uchun boshqa usuldan foydalanish
          const element = await this.driver.elementByXPath('//Text[text()="Касса успешно открытa"]');
      
          // Element topildi, uni ustida amal bajarish
          await element.click();
        } catch (error) {
          if (error.message.includes('element topilmadi')) {
            // Element topilmadi, Flashbar xabarni olish
            const flashbarElement = await this.driver.elementByXPath('//Text[@id="flashbar"]');
            const flashbarXabar = await flashbarElement.text();
            console.log('Flashbar Xabar:', flashbarXabar);
          } else {
            // Boshqa istisnolarni boshqarish
            console.error('Xato:', "Касса успешно открытa");
          }
        }
      
      //   const alertMessage = await this.driver.elementByName('Касса успешно открытa')
      //   const alertText = await alertMessage.text()

    
      //   if (alertText === 'Касса успешно открытa') {
      //     console.log('Alert text is correct.')
      //   } else {
      //     console.error(`Alert text is incorrect: ${alertText}`)
      //   }
      }

      async closeShift() {
        const cmena = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[1]");
        await cmena.click();
        await new Promise(resolve => setTimeout(resolve, 4000))
        const cmenastatus = await this.driver.elementByName('Открытый');
        await cmenastatus.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const createShift4Button = await this.driver.elementByName('Создать заявку')
        await createShift4Button.click()
        const coment = await this.driver.elementByName("Введите комментарии");
        await coment.sendKeys("enter test");
        const createShift1Button = await this.driver.elementByName('Отправить')
        await createShift1Button.click()
        await new Promise(resolve => setTimeout(resolve, 2000))
        const viewConsultantsButton = await this.driver.elementByName('Закрыть кассу')
        await viewConsultantsButton.click()
      //   const alertMessage = await this.driver.elementByName('Касса успешно открытa')
      //   const alertText = await alertMessage.text()

    
      //   if (alertText === 'Касса успешно открытa') {
      //     console.log('Alert text is correct.')
      //   } else {
      //     console.error(`Alert text is incorrect: ${alertText}`)
      //   }
      // }
      await new Promise(resolve => setTimeout(resolve, 3000))
      const buttonback = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group/Button[1]");
      await buttonback.click()
      await new Promise(resolve => setTimeout(resolve, 2000))

      }
  
      async filter() {
        await new Promise(resolve => setTimeout(resolve, 2000))
        let filterstatus = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group/Button[1]");
        await filterstatus.click();
        let filterstatus1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
        await filterstatus1.click();
        const esc = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
        await esc.click();
        const element1 = await this.driver.elementByName('Закрытый')
        const text1 = await element1.text()

        if (text1 === 'Закрытый') {
          console.log('Text content of "Закрытый" is correct.')
        } else {
          console.error(`Text content of 'Закрытый' is incorrect: ${text1}`)
        }

        // const createShiftStatus2 = await this.driver.elementByName('Дата открытия')
        // await createShiftStatus2.click()
        // const may = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[4]");
        // await may.click();
        // const may1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[8]");
        // await may1.click();
        // const prinyat = await this.driver.elementByName("Принять");
        // await prinyat.click();
        // const element = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[6]");
        // const text = await element.text()
      
        // if (text === '09.05.2023 14:27') {
        //   console.log('Text content of "09.05.2023 14:27" is correct.')
        // } else {
        //   console.error(`Text content of '09.05.2023 14:27' is incorrect: ${text}`)
        // }

        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByName("Да");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))


      }

      async negativeCmena() {
        const createShiftButton = await this.driver.elementByName('Создать смену')
        await createShiftButton.click()
        await new Promise(resolve => setTimeout(resolve, 3000))
      }

      async negativeBack() {
        const cmena = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[1]");
        await cmena.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
      }
      
      async negativeFilter() {
        // const filter = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group/Edit/Text");
        // await filter.click();
        // const filter0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[13]");
        // await filter0.click();
        // const filter1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[4]");
        // await filter1.click();
        const cmena = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[1]");
        await cmena.click();
        const cmenastatus = await this.driver.elementByName('В ожидании');
        await cmenastatus.click();
        await new Promise(resolve => setTimeout(resolve, 3000))
        const createShift2Button = await this.driver.elementByName('Открыть смену')
        await createShift2Button.click()
        const el1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Edit/Edit");
        await el1.sendKeys("1");
        const createShift3Button = await this.driver.elementByName('Отправить')
        await createShift3Button.click()
        await new Promise(resolve => setTimeout(resolve, 3000))
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByName("Да");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))

      }

      
  }
  
  module.exports = SmenaPage