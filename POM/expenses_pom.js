class ExpensesPage {
    constructor(driver) {
      this.driver = driver
    }

    async add_expenses () {
      const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      await expensesIcon.click();
      const addExpenses = await this.driver.elementByName('Добавить');
      await addExpenses.click();
      const addInput = await this.driver.elementByName("* Тип расхода");
      await addInput.click();
      const addInput0 = await this.driver.elementByName('Расход на обед');
      await addInput0.click();
      const addInput1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[1]");
      await addInput1.sendKeys("10000");
      const addInput2= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[2]");
      await addInput2.sendKeys("100000");
      const addExpenses2 = await this.driver.elementByName('Добавить');
      await addExpenses2.click();

    }

    async edit_expenses () {
      await new Promise(resolve => setTimeout(resolve, 3000));
      const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      await expensesIcon.click();
      const editButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group/Button[1]");
      await editButton.click();
      const comentedit = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[2]");
      await comentedit.sendKeys("test comment");
      const addExpenses2 = await this.driver.elementByName('Изменить');
      await addExpenses2.click();
      await new Promise(resolve => setTimeout(resolve, 3000));
      const element = await this.driver.elementByName('100000test comment');
      const text = await element.text();
  
      if (text === '100000test comment') {
        console.log(`Text content of '100000test comment' to change.`)
      } else {
        console.error(`Text content of '100000test comment' is incorrect: ${text}`)
      }
      
    }

    async expenses_filter () {
      // const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      // await expensesIcon.click();
      // const createShiftStatus2 = await this.driver.elementByName('Выберите дату')
      // await createShiftStatus2.click();
      // const may = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[4]");
      // await may.click();
      // const may1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[8]");
      // await may1.click();
      // const prinyat = await this.driver.elementByName("Принять");
      // await prinyat.click();

    }

    async delete_expenses () {
      const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      await expensesIcon.click();
      const deleteButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group/Button[2]");
      await deleteButton.click();
      const deleteyes = await this.driver.elementByName('Да, удалить');
      await deleteyes.click();

      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))

        
    }

    async add_expensesng () {
      const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      await expensesIcon.click();
      const addExpenses = await this.driver.elementByName('Добавить');
      await addExpenses.click();
      const addInput = await this.driver.elementByName("* Тип расхода");
      await addInput.click();
      const addInput0 = await this.driver.elementByName('Расход на обед');
      await addInput0.click();
      const addInput1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[1]");
      await addInput1.sendKeys(" ");
      const addInput2= await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group[2]/Group/Edit[2]");
      await addInput2.sendKeys("100000");
      const addExpenses2 = await this.driver.elementByName('Добавить');
      await addExpenses2.click();
      const element2 = await this.driver.elementByName('Введите сумму расхода');
      const text1 = await element2.text();
  
      if (text1 === 'Введите сумму расхода') {
        console.log(`Should be visualized validation error 'Введите сумму расхода'.`)
      } else {
        console.error(`Should be visualized validation error of 'Введите сумму расхода' is incorrect: ${text1}`)
      }

      const group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
      await group.click();

    }

    async negativeTest() {
      const expensesIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[6]");
      await expensesIcon.click();
      const addExpenses = await this.driver.elementByName('Добавить');
      await addExpenses.click();
      const addExpenses2 = await this.driver.elementByName('Добавить');
      await addExpenses2.click();
      const fields = [
        'Введите сумму расхода'
      ]
  
      for (const field of fields) {
        const element = await this.driver.elementByName(field)
        const text = await element.text();
  
        if (text === field) {
          console.log(`Should be visualized validation error ${field} .`)
        } else {
          console.error(`Should be visualized validation error ${field} is incorrect: ${text}`)
        }
      }

      const group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
      await group.click();

      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))


    }


}
  
module.exports = ExpensesPage