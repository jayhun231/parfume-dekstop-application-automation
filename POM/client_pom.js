class CostumPage {
    constructor(driver) {
      this.driver = driver
    }

    async create_new_client () {
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton.click();
      const addcostum = await this.driver.elementByName('Добавить')
      await addcostum.click();
      const lastName = await this.driver.elementByName("Введите Ф.И.О");
      await lastName.sendKeys("TesterQA");
      const phNumber = await this.driver.elementByName("Введите номер телефона");
      await phNumber.sendKeys("998991116");
      const bhDate = await this.driver.elementByName('Выберите дату рождения')
      await bhDate.click();
      const bhDate0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Text");
      await bhDate0.click();
      await bhDate0.click();
      await bhDate0.click();
      const bhDate1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[1]");
      await bhDate1.click();
      await bhDate1.click();
      const bhDate2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[2]");
      await bhDate2.click()
      const bhDate3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[22]");
      await bhDate3.click();
      const printDate = await this.driver.elementByName('Принять');
      await printDate.click();
      const pol = await this.driver.elementByName("Пол");
      await pol.click();
      const male = await this.driver.elementByName("Мужчина");
      await male.click();
      const addSave = await this.driver.elementByName('Сохранить');
      await addSave.click();


    }

    async client_page_filter () {
      await new Promise(resolve => setTimeout(resolve, 3000))
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton.click();
      const nameEnter = await this.driver.elementByName("Поиск по ФИО");
      await nameEnter.sendKeys("TestQA");
      const element = await this.driver.elementByName("TestQA");
      const text = await element.text()
  
      if (text === 'TestQA') {
        console.log(`Find correct client 'TestQA'.`)
      } else {
        console.error(`Text content of 'TestQA' is incorrect: ${text}`)
      }
      const costumButton1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton1.click();
      const phnumberEnter = await this.driver.elementByName("Поиск по номеру телефона");
      await phnumberEnter.sendKeys("998991114");
      const element2 = await this.driver.elementByName("+998 99 899 11 14");
      const text1 = await element2.text()
  
      if (text1 === '998991114') {
        console.log(`Find correct client '998991114'.`)
      } else {
        console.error(`Text content of '998991114' is incorrect: ${text}`)
      }

      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))


    }

    async enter_empty_space_name () {
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton.click();
      const addcostum = await this.driver.elementByName('Добавить')
      await addcostum.click();
      const lastName = await this.driver.elementByName("Введите Ф.И.О");
      await lastName.sendKeys("    ");
      const phNumber = await this.driver.elementByName("Введите номер телефона");
      await phNumber.sendKeys("998991116");
      const bhDate = await this.driver.elementByName('Выберите дату рождения')
      await bhDate.click();
      const bhDate0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Text");
      await bhDate0.click();
      await bhDate0.click();
      await bhDate0.click();
      const bhDate1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[1]");
      await bhDate1.click();
      await bhDate1.click();
      const bhDate2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[2]");
      await bhDate2.click()
      const bhDate3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[22]");
      await bhDate3.click();
      const printDate = await this.driver.elementByName('Принять');
      await printDate.click();
      const pol = await this.driver.elementByName("Пол");
      await pol.click();
      const male = await this.driver.elementByName("Мужчина");
      await male.click();
      const addSave = await this.driver.elementByName('Сохранить');
      await addSave.click();

      const element2 = await this.driver.elementByName('Введите Ф.И.О');
      const text1 = await element2.text();
  
      if (text1 === 'Введите Ф.И.О') {
        console.log(`Should be visualized validation error 'Введите Ф.И.О'.`)
      } else {
        console.error(`Should be visualized validation error of 'Введите Ф.И.О' is incorrect: ${text1}`);
      }

      const Group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
      await Group.click();


    }

    async enter_invalid_date_birth () {
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton.click();
      const addcostum = await this.driver.elementByName('Добавить')
      await addcostum.click();
      const lastName = await this.driver.elementByName("Введите Ф.И.О");
      await lastName.sendKeys("TesterQA");
      const phNumber = await this.driver.elementByName("Введите номер телефона");
      await phNumber.sendKeys("998991116");
      const bhDate = await this.driver.elementByName('Выберите дату рождения')
      await bhDate.click();
      const printDate = await this.driver.elementByName('Принять');
      await printDate.click();
      const pol = await this.driver.elementByName("Пол");
      await pol.click();
      const male = await this.driver.elementByName("Мужчина");
      await male.click();
      const addSave = await this.driver.elementByName('Сохранить');
      await addSave.click();

      const element2 = await this.driver.elementByName('Дата рождения не указана');
      const text1 = await element2.text()
  
      if (text1 === 'Дата рождения не указана') {
        console.log(`Should be visualized validation error 'Дата рождения не указана'.`)
      } else {
        console.error(`Should be visualized validation error of 'Дата рождения не указана' is incorrect: ${text1}`)
      }

      const Group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
      await Group.click();

    }

    async enter_invalid_length_phnumber () {
      const costumButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[4]");
      await costumButton.click();
      const addcostum = await this.driver.elementByName('Добавить')
      await addcostum.click();
      const lastName = await this.driver.elementByName("Введите Ф.И.О");
      await lastName.sendKeys("TesterQA");
      const phNumber = await this.driver.elementByName("Введите номер телефона");
      await phNumber.sendKeys("99899111");
      const bhDate = await this.driver.elementByName('Выберите дату рождения')
      await bhDate.click();
      const bhDate0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Text");
      await bhDate0.click();
      await bhDate0.click();
      await bhDate0.click();
      const bhDate1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[1]");
      await bhDate1.click();
      await bhDate1.click();
      const bhDate2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Text[2]");
      await bhDate2.click()
      const bhDate3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[22]");
      await bhDate3.click();
      const printDate = await this.driver.elementByName('Принять');
      await printDate.click();
      const pol = await this.driver.elementByName("Пол");
      await pol.click();
      const male = await this.driver.elementByName("Мужчина");
      await male.click();
      const addSave = await this.driver.elementByName('Сохранить');
      await addSave.click();

      const element2 = await this.driver.elementByName('Номер неверный или неполный');
      const text1 = await element2.text();
  
      if (text1 === 'Номер неверный или неполный') {
        console.log(`Should be visualized validation error 'Номер неверный или неполный'.`)
      } else {
        console.error(`Should be visualized validation error of 'Номер неверный или неполный' is incorrect: ${text1}`)
      }

      const Group = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group");
      await Group.click();
      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByName("Да");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))

    }

}
  
module.exports = CostumPage