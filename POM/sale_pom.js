class SmenaPage {
    constructor(driver) {
      this.driver = driver
    }

    async viewsalepage() {
        const viewSaleButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
        await viewSaleButton.click()
        await new Promise(resolve => setTimeout(resolve, 5000))
    }
    
    // async verifyFields() {
    //   const fields = [
    //     'Номер чека',
    //     'Смена',
    //     'Клиент',
    //     'Старый клиент',
    //     'Консультант',
    //     'Дата',
    //     'Статус',
    //     'Сертификат',
    //   ]
  
    //   for (const field of fields) {
    //       const element = await this.driver.elementByName(field);
    //       const text = await element.text();
    //       if (text === field) {
    //         console.log(`Text content of ${field} is correct.`);
    //       } else {
    //         console.error(`Text content of ${field} is incorrect: ${text}`);
    //       }
    //     }
    // }

    async viewsaleget() {
      const viewSaleButton1 = await this.driver.elementByName('Создать продажу')
      await viewSaleButton1.click()
      await new Promise(resolve => setTimeout(resolve, 5000))
      const viewSaleInput = await this.driver.elementByName('Отсканируйте штрих-код')
      await viewSaleInput.sendKeys("3348901420402")
      await new Promise(resolve => setTimeout(resolve, 6000))
      await viewSaleInput.sendKeys("3348901420402")
      await new Promise(resolve => setTimeout(resolve, 5000))
      // const fields = [
      //   '1 BN011A02 EDP Spray 50ml 1 816 000 Фикс 16 000 16 000 800 000',
      //   '2 BN004A01 EDT Spray 100ml 1 924 000 Процент 50 462 000 462 000',
      // ]
      // for (const field of fields) {
      //   const element = await this.driver.elementByName(field);
      //   const text = await element.text();
      //   if (text === field) {
      //     console.log(`Should be added count to existing product with ${field} Штрих Код at product list `);
      //   } else {
      //     console.error(`Should be not added count to existing product with ${field} Штрих Код at product list`);
      //   }
      // }
      const el1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Button[1]");
      await el1.click();
      const el2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await el2.click();
      const viewSaleButton2 = await this.driver.elementByName('Совершить продажу')
      await viewSaleButton2.click();
      await new Promise(resolve => setTimeout(resolve, 5000))
      await new Promise(resolve => setTimeout(resolve, 6000)) 
      const element = await this.driver.elementByName('Завершён');
      const text = await element.text();
      if (text === 'Завершён') {
        console.log(`Status changed to 'Завершён'`);
      } else {
        console.error(`Status not changed to 'Завершён'`);
      }
      
    }
    
    async viewsaleget1() {
      const viewSaleButton1 = await this.driver.elementByName('Создать продажу')
      await viewSaleButton1.click()
      await new Promise(resolve => setTimeout(resolve, 6000))
      const sertificat = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/CheckBox");
      await sertificat.click();
      await new Promise(resolve => setTimeout(resolve, 5000))
      const viewSaleInput = await this.driver.elementByName('Отсканируйте штрих-код')
      await viewSaleInput.sendKeys("pa-800119")
      await new Promise(resolve => setTimeout(resolve, 5000))
      // const fields = [
      //   'AA-100119',
      // ]
      // for (const field of fields) {
      //   const element = await this.driver.elementByName(field);
      //   const text = await element.text();
      //   if (text === field) {
      //     console.log(`Should be added count to existing product with ${field} Штрих Код at product list `);
      //   } else {
      //     console.error(`Should be not added count to existing product with ${field} Штрих Код at product list`);
      //   }
      // }
      const konsul = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Button[1]");
      await konsul.click();
      const select = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await select.click();
      const el1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Button[2]");
      await el1.click();
      const el2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await el2.click();
      const viewSaleButton2 = await this.driver.elementByName('Совершить продажу')
      await viewSaleButton2.click();
      await new Promise(resolve => setTimeout(resolve, 3000)) 
      const viewSaleButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
      await viewSaleButton.click()
      await new Promise(resolve => setTimeout(resolve, 6000))
      const element = await this.driver.elementByName('Завершён');
      const text = await element.text();
      if (text === 'Завершён') {
        console.log(`Status changed to 'Завершён'`);
      } else {
        console.error(`Status not changed to 'Завершён'`);
      }
    }
    async viewfilter() {
      const el1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group/Button[1]");
      await el1.click();
      const el18 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await el18.click();
      const el3 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
      await el3.click();
      const element = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[5]");
      const text = await element.text();
        if (text === 'В процессе') {
          console.log(`Text content of 'В процессе' is correct.`);
        } else {
          console.error(`Text content of 'В процессе' is incorrect: ${text}`);
        }
        // const createShiftStatus2 = await this.driver.elementByName('Выберите дату')
        // await createShiftStatus2.click()
        // const may = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[2]");
        // await may.click();
        // const may1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[10]");
        // await may1.click();
        // const prinyat = await this.driver.elementByName("Принять");
        // await prinyat.click();
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))
    }

    async viewsalegetng() {
      const viewSaleButton1 = await this.driver.elementByName('Создать продажу')
      await viewSaleButton1.click()
      await new Promise(resolve => setTimeout(resolve, 5000))
      const viewSaleInput = await this.driver.elementByName('Отсканируйте штрих-код')
      await viewSaleInput.sendKeys("11111112222222")
      const viewSaleButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
      await viewSaleButton.click()
      await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async viewsalegetng1() {
      const viewSaleButton1 = await this.driver.elementByName('Создать продажу')
      await viewSaleButton1.click()
      await new Promise(resolve => setTimeout(resolve, 5000))
      let sertificat = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Group[1]/CheckBox");
      await sertificat.click();
      await new Promise(resolve => setTimeout(resolve, 5000))
      const getnum = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Edit");
      await getnum.sendKeys(" C-0000000")
      await new Promise(resolve => setTimeout(resolve, 5000))
      const viewSaleButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
      await viewSaleButton.click()
      await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async negativeFilter() {
      // // const createShiftStatus2 = await this.driver.elementByName('Выберите дату')
      // // await createShiftStatus2.click()
      // // const data = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[17]");
      // // await data.click();
      // // await data.click();
      // // const prinyat = await this.driver.elementByName("Принять");
      // // await prinyat.click();
      // const element = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Text[6]");
      // const text = await element.text()
    
      // if (text === '17.05.2023 12:13') {
      //   console.log('Text content of "17.05.2023 12:13" is correct.')
      // } else {
      //   console.error(`Text content of '17.05.2023 12:13' is incorrect: ${text}`)
      // }
      
      const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
      await logout.click();
      await new Promise(resolve => setTimeout(resolve, 2000))
      const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
      await logout1.click(); 
      await new Promise(resolve => setTimeout(resolve, 2000))
    }

    async isButtonDisabled(buttonName) {
      const button = await this.driver.elementByName(buttonName);
      let tries = 0;
      while (tries < 3) {
        let isEnabled = await button.isEnabled();
        if (!isEnabled) {
          return true; // button is disabled
        }
        console.log('Button is disabled: ');
        await new Promise(resolve => setTimeout(resolve, 1000));
        tries++;
      }
      return true;
    }

    async viewsaleget2() {
      const viewSaleButton = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[2]");
      await viewSaleButton.click()
      await new Promise(resolve => setTimeout(resolve, 5000))
      const viewSaleButton1 = await this.driver.elementByName('Создать продажу')
      await viewSaleButton1.click()
      const viewSaleInput = await this.driver.elementByName('Отсканируйте штрих-код')
      await viewSaleInput.sendKeys("3348901420402")
      await new Promise(resolve => setTimeout(resolve, 6000))
      await viewSaleInput.sendKeys("3348901420402")
      await new Promise(resolve => setTimeout(resolve, 5000))
      const konsul = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Group/Button[1]");
      await konsul.click();
      const konsul1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await konsul1.click();
      const sertify = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[1]/Group[2]/Text[2]");
      await sertify.click();
      const sertify0 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group/Image/Button");
      await sertify0.click();
      const sertify1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group");
      await sertify1.click();
      const sertify2 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group");
      await sertify2.click();
      const viewSaleButton2 = await this.driver.elementByName('Совершить продажу')
      await viewSaleButton2.click();
      await new Promise(resolve => setTimeout(resolve, 5000))
      await new Promise(resolve => setTimeout(resolve, 6000)) 
      const element = await this.driver.elementByName('Завершён');
      const text = await element.text();
      if (text === 'Завершён') {
        console.log(`Status changed to 'Завершён'`);
      } else {
        console.error(`Status not changed to 'Завершён'`);
      }

    }
    
}
  
module.exports = SmenaPage