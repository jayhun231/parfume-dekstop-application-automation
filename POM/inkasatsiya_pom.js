class InkasatsiyaPage {
    constructor(driver) {
      this.driver = driver
    }

    async filter_positive_case () {
        const inkasatsiyaIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Text[7]");
        await inkasatsiyaIcon.click();
        await new Promise(resolve => setTimeout(resolve, 3000));
        // const createShiftStatus2 = await this.driver.elementByName('Выберите дату');
        // await createShiftStatus2.click();
        // await new Promise(resolve => setTimeout(resolve, 3000));
        // const may = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[4]");
        // await may.click();
        // const may1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[8]");
        // await may1.click();
        // const prinyat = await this.driver.elementByName("Принять");
        // await prinyat.click();
        await new Promise(resolve => setTimeout(resolve, 5000));
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000))
        const logout1 = await this.driver.elementByName("Да");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000))

    }

    async filter_negative_case () {
        const inkasatsiyaIcon = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Text[6]");
        await inkasatsiyaIcon.click();
        await new Promise(resolve => setTimeout(resolve, 3000));
        // const createShiftStatus2 = await this.driver.elementByName('Выберите дату');
        // await createShiftStatus2.click();
        // await new Promise(resolve => setTimeout(resolve, 3000))
        // const date = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Group/Group[2]/Group[2]/Text[18]");
        // await date.click();
        // const prinyat = await this.driver.elementByName("Принять");
        // await prinyat.click();
        await new Promise(resolve => setTimeout(resolve, 5000));
        const logout = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button");
        await logout.click();
        await new Promise(resolve => setTimeout(resolve, 2000));
        const logout1 = await this.driver.elementByXPath("/Window/Pane/Group/Group/Group/Group/Group/Button[2]");
        await logout1.click(); 
        await new Promise(resolve => setTimeout(resolve, 2000));

    }


}
  
module.exports = InkasatsiyaPage