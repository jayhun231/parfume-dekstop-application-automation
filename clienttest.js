const wd = require('wd');
const LogenPage = require('./POM/logenPage_pom');
const CostumPage = require('./POM/client_pom');

class MyTest {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.costumPage = new CostumPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runClientTest() {
    console.log('Starting customer test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    //  await this.costumPage.create_new_client();
    await this.costumPage.client_page_filter();
    await this.logenPage.close();
    console.log('Client Positive test completed!');
  }
}

module.exports = MyTest;