const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const KonsultantPagePage = require('./POM/konsultant_pom');

class KonsultantngTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.konsultantPage = new KonsultantPagePage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runKonsultantngTest() {
    console.log('Starting Konsultant negative positive test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.konsultantPage.creat_new_konsultantng ()
    await this.logenPage.close();
  
  


  console.log('Konsultant negative test completed!')
}
}

module.exports = KonsultantngTestCaller;