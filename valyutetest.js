const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const ValyutaPage = require('./POM/valyuta_pom')

class ValyuteTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.valyutaPage = new ValyutaPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runValyuteTest() {
    console.log('Starting Valyute test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.valyutaPage.valyuta_add()
    await this.valyutaPage.filter_valute()
    await this.logenPage.close();
    console.log('Valyute POSITIVE test completed!');
  }
}

module.exports = ValyuteTestCaller;