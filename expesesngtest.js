const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const ExpensesPage = require('./POM/expenses_pom')

class ExpensesngTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.expensesPage = new ExpensesPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runExpensesngTest() {
    console.log('Starting Expenses positive test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.expensesPage.add_expensesng()
    await this.expensesPage.negativeTest()

    await this.logenPage.close()
    console.log('Expenses Negative test completed!')
}
}

module.exports = ExpensesngTestCaller;