const wd = require('wd');
const LogenPage = require('./POM/logenPage_pom');
const SalePage = require('./POM/sale_pom');

class SaleTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.salePage = new SalePage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runSaleTest() {
    console.log('Starting sale test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.salePage.viewsalepage();
    // await this.salePage.verifyFields();
    await this.salePage.viewsaleget();
    await this.salePage.viewsaleget2();
    await this.salePage.viewsaleget1();
    await this.salePage.viewfilter();
    await this.logenPage.close();
    console.log('Sale test completed!');
  }
}

module.exports = SaleTestCaller;