const wd = require('wd');
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom');
const SalePage = require('./POM/sale_pom');

class SaleTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.salePage = new SalePage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runSaleNegativeTest() {
    console.log('Starting sale negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.salePage.viewsalepage();
    await this.salePage.viewsalegetng();
    await this.salePage.viewsalegetng1();
    const expectedValue = true; // expected button state should be disabled
    const actualValue1 = await this.salePage.isButtonDisabled('Применить'); // replace buttonName with the name of your button
    assert.strictEqual(actualValue1, expectedValue);
    await this.salePage.negativeFilter();
    await this.logenPage.close();
    console.log('Sale Negative test completed!');
  }
}

module.exports = SaleTestCaller;