const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const VozvratPage = require('./POM/vozvrat_pom')

class VozvratngTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.vozvratPage = new VozvratPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runVozvratngTest() {
    console.log('Starting Vozvrat Negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.vozvratPage.negative_vozvrat()
    await this.vozvratPage.delite_vozvrat()
    await this.logenPage.close();
    console.log('Vozvrat Negative test completed!');
  }
}

module.exports = VozvratngTestCaller;