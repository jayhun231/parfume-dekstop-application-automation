const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const SpisanyaPage = require('./POM/spisanya_pom')

class SpisanyaTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.spisanyaPage = new SpisanyaPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runSpisanyaTest() {
    console.log('Spisanya smena test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.spisanyaPage.add_spisanya()
    await this.spisanyaPage.filter_spisanya()
    await this.logenPage.close();
    console.log('Spisanya test completed!');
  }
}

module.exports = SpisanyaTestCaller;