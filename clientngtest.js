const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const CostumPage = require('./POM/client_pom');

class ClientTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.costumPage = new CostumPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runClientngTest() {
    console.log('Starting Client Negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.costumPage.enter_empty_space_name()
    await this.costumPage.enter_invalid_date_birth()
    await this.costumPage.enter_invalid_length_phnumber()
    

    await this.logenPage.close();
  
  


  console.log('Client negative test completed!')
}
}

module.exports = ClientTestCaller;