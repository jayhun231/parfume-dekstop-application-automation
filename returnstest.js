const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const ReturnsPage = require('./POM/returns_pom')

class ReturnsTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.returnsPage = new ReturnsPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runReturnsTest() {
    console.log('Starting Returns test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.returnsPage.creat_new_returns()
    await this.returnsPage.continue_returns()
    await this.returnsPage.save()
    await this.returnsPage.filter_positive()
    await this.logenPage.close();
    console.log('Returns test completed!');
  }
}

module.exports = ReturnsTestCaller;