const wd = require('wd');
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom');
const SmenaPage = require('./POM/smena_pom');

class SmenaTestCaller0 {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.smenaPage = new SmenaPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runSmenaNegativeTest() {
    console.log('Starting smena negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.smenaPage.negativeCmena();
    const expectedValue = true; // expected button state should be disabled
    const actualValue1 = await this.smenaPage.isButtonDisabled('Создать заявку'); // replace buttonName with the name of your button
    assert.strictEqual(actualValue1, expectedValue);
    await this.smenaPage.negativeBack();
    await this.smenaPage.negativeFilter();
    await this.logenPage.close();
    console.log('Smena Negative test completed!');
  }
}

module.exports = SmenaTestCaller0;