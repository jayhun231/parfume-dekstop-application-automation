const wd = require('wd')
const LogenPage = require('./POM/logenPage_pom')
const PeremeshinPage = require('./POM/peremeshen_pom')

class PerimeshinTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.perimeshinPage = new PeremeshinPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runPermishenTest() {
    console.log('Starting Perimesheniya test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.perimeshinPage.add_permeshin();
    await this.perimeshinPage.prinyat();
    await this.perimeshinPage.permeshin_filter();
    await this.logenPage.close();
    console.log('Peremesheniya test completed!');
  }
}

module.exports = PerimeshinTestCaller;