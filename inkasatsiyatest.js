const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const InkasatsiyPage = require('./POM/inkasatsiya_pom')

class InkasatsiyaTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.inkasatsiyaPage = new InkasatsiyPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runInkasatsiyaTest() {
  console.log('Starting Inkasatsiya test...')

  await this.logenPage.open();
  await this.logenPage.login(this.username, this.password);
  await this.inkasatsiyaPage.filter_positive_case()
  await this.logenPage.close();
  console.log('Inkasatsiya test completed!');
}
}

module.exports = InkasatsiyaTestCaller;