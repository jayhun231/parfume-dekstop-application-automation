const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const ValyutaPage = require('./POM/valyuta_pom')

class ValyutengTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.valyutaPage = new ValyutaPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runValyutengTest() {
    console.log('Starting Valyute Negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.valyutaPage.negative_add()
    await this.logenPage.close();
    console.log('Valyute Negative test completed!');
  }
}

module.exports = ValyutengTestCaller;