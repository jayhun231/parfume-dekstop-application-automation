const wd = require('wd')
const assert = require('assert');
const LogenPage = require('./POM/logenPage_pom')
const VozvratPage = require('./POM/vozvrat_pom')

class VozvratTestCaller {
  constructor() {
    this.driver = wd.promiseChainRemote('http://127.0.0.1:4723/');
    this.logenPage = new LogenPage(this.driver);
    this.vozvratPage = new VozvratPage(this.driver);
    this.username = 'medion_admin';
    this.password = 'admin_medion123';
  }

  async runVozvratTest() {
    console.log('Starting Vozvrat Negative test...');
    await this.logenPage.open();
    await this.logenPage.login(this.username, this.password);
    await this.vozvratPage.click_icon()
    // await this.vozvratPage.verifyFields()
    // await this.vozvratPage.click_table()
    await this.vozvratPage.add_vozvrat()
    await this.vozvratPage.delite_vozvrat()
    await this.logenPage.close();
    console.log('Vozvrat test completed!');
  }
}

module.exports = VozvratTestCaller;